Link: https://gitlab.upt.ro/valeria.rus/generator-de-car-i-de-pove-ti-utilizand-metode-de-inva-are-automata/

Pasi:
1. Se instaleaza interpretorul Anaconda.
2. Se creeaza un environment in Anaconda prin comanda "conda create --name myenv".
3. Se activeaza interpretorul prin comanda "activate myenv".
4. Se instaleaza bibliotecile din requirements.txt prin comanda "pip install -r localpath/requirements.txt", unde localpath este locatia proiectului.
5. Se instaleaza stable-diffusion prin comnda "pip install https://github.com/CompVis/stable-diffusion".
6. Proiectul este rulat prin comanda python main.py din locatia proiectului.
