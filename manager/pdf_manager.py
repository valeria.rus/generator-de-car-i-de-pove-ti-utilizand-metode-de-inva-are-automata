
class PDFparser:
    def __init__(self, reader):
        self.reader = reader    # init pdf reader
        # self.bookmark_dict = reader.getOutlines()

    def bookmark_dict(self, bookmark_list):
        """Find all bookmarks with their pages"""
        result = {}  # result json as page: title -> ex: [6: Chapter 2..., 12: Chapter 3...]
        bookmark_dict = []  # dict
        for item in bookmark_list:
            if isinstance(item, list):
                # recursive call
                result.update(bookmark_dict)
            else:
                result[self.reader.get_destination_page_number(item)] = item.title  # write result in dict page: chapter
        return result

    @staticmethod
    def filter_extracted_chapters(chapter_list):
        """Create new chapter filtered list"""
        new_chapter_list = {}
        for chapter in chapter_list:
            if chapter_list[chapter] != 'Contents' and chapter_list[chapter] != 'Introduction':  # filter
                new_chapter_list[chapter] = chapter_list[chapter]  # add only the chapters
        return new_chapter_list

    def dict_chapter_to_text(self, chapter_list):
        result = {}  # result json as title: text -> ex: [Chapter 2...: Dorothy is..., Chapter 3...: The wizard..]
        last_pdf_page = len(self.reader.pages)  # total number of pages in pdf (the last page number)
        no_chapters = len(chapter_list)     # total number of chapters
        chapter_page_key = list(chapter_list.keys())    # list with the chapter pages (from 0 to length-1)
        chapter_counter = 0
        for chapter_page in chapter_list:
            chapter_counter += 1    # count chapters
            if chapter_counter == no_chapters:  # if we are at the last chapter
                result[chapter_list[chapter_page]] = self.extract_chapter_text(chapter_page, last_pdf_page)  # we use the last pdf page to extract the text
            else:
                result[chapter_list[chapter_page]] = self.extract_chapter_text(chapter_page, chapter_page_key[chapter_counter])  # current chapter page and next chapter page
        return result

    def extract_chapter_text(self, first_page, last_page):
        chapter_text = ""   # full chapter text
        for page in range(first_page, last_page):   # loop through pages
            chapter_text = chapter_text + self.reader.pages[page].extract_text()   # append page to create the full chapter text
        return chapter_text

    def extract_chapter_with_text(self):
        """Main function to extract the exact text by chapter"""
        chapter_list = self.bookmark_dict(self.reader.outline)  # parse pdf to extract chapters page and their titles
        filtered_list = self.filter_extracted_chapters(chapter_list)    # filter raw list
        chapter_text_list = self.dict_chapter_to_text(filtered_list)    # create title: text list
        return chapter_text_list
