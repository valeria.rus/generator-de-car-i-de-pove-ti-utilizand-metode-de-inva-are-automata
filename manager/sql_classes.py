from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(100))
    parent = db.Column(db.Boolean)
    # we use this for to know which parent has which kid (parent value is none, kid value is parent's _id)
    parent_id = db.Column(db.Integer)
    book_list = db.Column(db.String(100))

    def __init__(self, username, password, parent, parent_id, book_list):
        self.username = username
        self.password = password
        self.parent = parent
        self.parent_id = parent_id
        self.book_list = book_list

    def __repr__(self):
        return f'<User {self._id, self.username, self.password, self.parent, self.parent_id, self.book_list}>'

    def get_id(self):
        return self._id


class Book(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    chapter_list = db.Column(db.String(500))  # list of chapter id's
    age = db.Column(db.String(100))

    def __init__(self, title, chapter_list, age):
        self.title = title
        self.chapter_list = chapter_list
        self.age = age

    def __repr__(self):
        return f'<Book {self._id, self.title, self.chapter_list, self.age}>'

    def get_id(self):
        return self._id


class Chapter(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    body = db.Column(db.String(500))
    image = db.Column(db.String(100))
    book_id = db.Column(db.Integer)

    def __init__(self, title, body, image, book_id):
        self.title = title
        self.body = body
        self.image = image
        self.book_id = book_id

    def __repr__(self):
        return f'<Chapter {self._id, self.title, self.body, self.image, self.book_id}>'

    def get_id(self):
        return self._id
