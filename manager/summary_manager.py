from transformers import BartTokenizer, BartForConditionalGeneration


class SummaryManager:
    def __init__(self, chapter_list, age):
        self.chapter_list = chapter_list
        # Load BART tokenizer and model
        self.tokenizer = BartTokenizer.from_pretrained('facebook/bart-large-cnn')
        self.model = BartForConditionalGeneration.from_pretrained('facebook/bart-large-cnn')
        self.age = age

    def chapter_summary(self, body):
        # Tokenize the input text
        inputs = self.tokenizer(body, max_length=1024, truncation=True, return_tensors='pt')

        if self.age == "6 - 9":
            # Generate the summary for young age
            summary_ids = self.model.generate(inputs['input_ids'], num_beams=4, max_length=500, early_stopping=True)
        else:
            # Generate the summary for older age
            summary_ids = self.model.generate(inputs['input_ids'], num_beams=4, min_length=10, max_length=200,
                                              temperature=5.0, early_stopping=False)
        summary = self.tokenizer.decode(summary_ids[0], skip_special_tokens=True)
        return summary

    def create_chapter_list(self):
        result = {}  # result json as title: summary -> ex: [Chapter 2...: summary, Chapter 3...: summary]
        for key in self.chapter_list:
            result[key] = self.chapter_summary(self.chapter_list[key])  # add value to json with summarization
            print(key)

        return result
