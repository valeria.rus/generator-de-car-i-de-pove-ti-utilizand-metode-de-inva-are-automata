import subprocess


class ImageGeneratorManager:
    """Generates image from text"""
    def __init__(self, text, path, characteristic_param, plms_param, skip_grid_param, n_samples_param, outdir_param):
        """Init"""
        self.command = [
            "python", path,
            "--prompt", text,
            characteristic_param,
            plms_param,
            skip_grid_param,
            n_samples_param,
            outdir_param
        ]

    def generate_img(self):
        """Generate image"""
        result = subprocess.run(self.command, capture_output=True, text=True)
        return result
