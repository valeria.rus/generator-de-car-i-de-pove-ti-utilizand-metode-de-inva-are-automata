from flask import Flask, render_template, request, redirect, url_for, session, g
from flask_sqlalchemy import SQLAlchemy
from manager import PDFparser, SummaryManager, ImageGeneratorManager, db, User, config_db_for_start, Book, Chapter
import PyPDF2
import os

app = Flask(__name__)
app.config["DEBUG"] = True

default_image = True    # default for image generation

app.config['SECRET_KEY'] = 'MlK_V9yz_7qhAfZv2OqQ7xgnqjCno9dRPPJkdpZVvbtlxdyE88c3PO'  # secret key used for user session
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False    # sql config
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///Database.sqlite'

db.init_app(app)

users = []
books = []
chapters = []


def get_parent_book_list(user):
    global books
    book_list = []
    user_book_list = user.book_list.split(";")  # split books id
    books = Book.query.all()    # update books
    for book_id in user_book_list:
        for book in books:
            if book_id == str(book.get_id()):
                book_list.append(book)
    return book_list


@app.before_request
def before_request():
    g.user = None

    if 'user_id' in session:
        global users
        users = User.query.all()
        current_user = [user for user in users if user.get_id() == session['user_id']][0]
        g.user = current_user


@app.route("/")
def home():
    """Home page"""
    if not g.user:  # if the user is not logged in
        return redirect(url_for('login'))
    return render_template('home.html', name=g.user.username, logged_in=True, is_parent=g.user.parent)


@app.route("/books")
def books():
    """Home page"""
    if not g.user:  # if the user is not logged in
        return redirect(url_for('login'))
    book_list = get_parent_book_list(g.user)
    return render_template('books.html', books=book_list, logged_in=True, is_parent=g.user.parent)


@app.route("/settings")
def settings():
    """Home page"""
    if not g.user:  # if the user is not logged in
        return redirect(url_for('login'))
    children_list = []
    for user in users:
        if not user.parent:     # if it is not parent
            if user.parent_id == g.user.get_id():
                children_list.append(user)
    parent_book_list = get_parent_book_list(g.user)
    return render_template('settings.html', children_list=children_list, book_list=parent_book_list, logged_in=True, is_parent=g.user.parent)


@app.route('/logout')
def logout():
    if not g.user:
        return redirect(url_for('login'))
    session.pop('user_id', None)    # remove user session and log out
    return redirect(url_for('login', logged_in=True, is_parent=g.user.parent))


@app.route("/login", methods=['POST', 'GET'])
def login():
    """Login page"""
    if request.method == 'POST':    # is login
        session.pop('user_id', None)    # reset session
        username = request.form['username']     # get user and password
        password = request.form['password']
        for user in users:
            if user.username == username:
                if user.password == password:
                    session['user_id'] = user.get_id()    # create new session
                    return redirect(url_for('home'))
        return redirect(url_for('login'))
    if g.user:  # we are already logged in
        return redirect(url_for('home'))
    return render_template('login.html')


@app.route('/generate_story', methods=['POST'])
def generate_story():
    """Start story processing (generate story)"""
    if not g.user:
        return redirect(url_for('login'))
    book_title = ""
    file = request.files['pdfFile']  # read input file
    age = request.form["age"]
    file.save(file.filename)  # save the file locally
    book_title = file.filename  # book title
    pdf_reader = PyPDF2.PdfReader(file.filename)  # pdf reader
    pdf_parser = PDFparser(pdf_reader)  # pdf manager
    result = pdf_parser.extract_chapter_with_text()  # extract chapters with text

    summary_manager = SummaryManager(result, age)  # summary manager (nlp manager)
    summary = summary_manager.create_chapter_list()  # create chapter list from nlp
    if not default_image:
        for chapter_key in summary:
            image_generator = ImageGeneratorManager(summary[chapter_key], "path/scripts/txt2img.py", "--characteristic_param",
                                                    "--plms", "--skip_grid_param", "--n_samples_param", "--outdir_param")  # image generator
            image_generator.generate_img()  # generate image for chapter text

    new_chapter_list = ""   # create new book chapter list
    first_chapter = True
    new_book = Book(os.path.splitext(book_title)[0], "", age)    # create new book
    db.session.add(new_book)  # add book
    db.session.flush()  # flushes changes to db
    for item in summary:
        new_chapter = Chapter(item, summary[item], "", new_book.get_id())   # create new chapter
        db.session.add(new_chapter)  # add chapter to db
        db.session.flush()  # flushes changes to db
        new_chapter.image = str(new_book.get_id()) + "_" + str(new_chapter.get_id())    # add image to chapter
        if first_chapter:
            new_chapter_list = str(new_chapter.get_id())  # add the first chapter to the list
            first_chapter = False
        else:
            new_chapter_list = new_chapter_list + ";" + str(new_chapter.get_id())  # add chapter to the list

    new_book.chapter_list = new_chapter_list    # update chapter list
    user = User.query.get(g.user.get_id())  # get user
    user_book_list = user.book_list
    user_book_list = str(user_book_list) + ";" + str(new_book.get_id())   # update book list
    user.book_list = user_book_list
    db.session.commit()     # update db
    return redirect(url_for('books'))


@app.route('/view_story', methods=['GET', 'POST'])    # favicon.ico error so we only declare as int
def view_story():
    if not g.user:
        return redirect(url_for('login'))
    if request.method == 'POST':
        selected_book_id = request.form['selected_book']
        chapter_number = request.form['chapter_number']
        last_chapter = request.form['last_chapter']
        first_chapter = request.form['first_chapter']
        action = request.form['submit_action']
        if action == "Sfarsit":  # we exit the story
            return redirect(url_for('books'))
        if action == "Pagina anterioara":   # back page
            if last_chapter == chapter_number:  # last page fix
                temp_chapter_number = str(int(chapter_number) - 1)
            else:   # every other page
                temp_chapter_number = str(int(chapter_number) - 2)
            if temp_chapter_number < first_chapter:
                temp_chapter_number = "0"
            if temp_chapter_number == "0":  # title page
                book = Book.query.get(selected_book_id)
                title = book.title
                next_chapter_number = str(int(chapter_number) - 1)
                chapter_number = "0"
                body = ""
                image = ""
            elif chapter_number == last_chapter:  # last page
                selected_chapter = Chapter.query.get(temp_chapter_number)
                title = selected_chapter.title
                body = selected_chapter.body
                next_chapter_number = temp_chapter_number
                image = selected_chapter.image
            else:   # normal page
                selected_chapter = Chapter.query.get(temp_chapter_number)
                title = selected_chapter.title
                body = selected_chapter.body
                image = selected_chapter.image
                next_chapter_number = str(int(temp_chapter_number) + 1)
        else:   # next page
            if chapter_number == "0":   # title page
                book = Book.query.get(selected_book_id)
                title = book.title
                book_chapters = book.chapter_list.split(";")
                next_chapter_number = book_chapters[0]
                first_chapter = book_chapters[0]
                last_chapter = book_chapters[-1]
                body = ""
                image = ""
            elif chapter_number == last_chapter:    # last page
                selected_chapter = Chapter.query.get(chapter_number)
                title = selected_chapter.title
                body = selected_chapter.body
                next_chapter_number = chapter_number
                image = selected_chapter.image
            else:   # every other page
                selected_chapter = Chapter.query.get(chapter_number)
                title = selected_chapter.title
                body = selected_chapter.body
                next_chapter_number = str(int(chapter_number) + 1)
                image = selected_chapter.image

        return render_template("view_story.html", title=title, chapter_number=chapter_number, next_chapter_number=next_chapter_number,
                               first_chapter=first_chapter, last_chapter=last_chapter, body=body, selected_book=selected_book_id,
                               image=image, logged_in=True, is_parent=g.user.parent)
    return redirect(url_for('books'))


if __name__ == '__main__':
    with app.app_context():  # config demo
        db.drop_all()   # drop old db
        db.create_all()  # create new db
        config_db_for_start(db)  # create db from config
        users = User.query.all()  # get all users
        books = Book.query.all()    # get all books
        chapters = Chapter.query.all()  # get all chapters
    app.run(host='0.0.0.0')
